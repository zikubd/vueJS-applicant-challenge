<?php

namespace NurulUmbhiya\VueApp;

use NurulUmbhiya\VueApp\Admin\Settings;
use NurulUmbhiya\VueApp\Rest\Controller;
use NurulUmbhiya\VueApp\Traits\AssetHelper;
use NurulUmbhiya\VueApp\Traits\ChainableContainer;
use NurulUmbhiya\VueApp\Traits\Singleton;

// don't call the file directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Bootstrap
 *
 * @since   1.0.0
 *
 * @property Settings   $settings
 * @property Assets     $assets
 * @property Controller $rest
 * @property Admin\Menu $menu
 *
 * @package NurulUmbhiya\VueApp
 */
final class Bootstrap {

	use ChainableContainer;
	use Singleton;
	use AssetHelper;

	/**
	 * Plugin version
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public string $version = '1.0.0';

	/**
	 * Minimum PHP version required
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	private string $min_php = '7.4';

	/**
	 * Bootstrap constructor.
	 *
	 * Sets up all the appropriate hooks and actions
	 * within our plugin.
	 *
	 * @since 1.0.0
	 */
	private function __construct() {
		register_activation_hook( $this->get_file(), array( $this, 'activate' ) );
		register_deactivation_hook( $this->get_file(), array( $this, 'deactivate' ) );

		add_action( 'plugins_loaded', array( $this, 'load_classes' ) );
		add_action( 'plugins_loaded', array( $this, 'load_hooks' ) );
	}

	/**
	 * Plugin activation hook
	 *
	 * @since 1.0.0
	 *
	 * @param $network_wide
	 *
	 * @return void
	 */
	public function activate( $network_wide ) {
		$settings_obj = new Settings();

		if ( is_multisite() && $network_wide ) {
			// get all blog ids
			$blog_ids = get_sites( array( 'fields' => 'ids' ) );

			foreach ( $blog_ids as $blog_id ) {
				switch_to_blog( $blog_id );

				$settings = get_option( Settings::OPTION_KEY, array() );
				if ( empty( $settings ) ) {
					// update settings
					update_option( Settings::OPTION_KEY, $settings_obj->get_default_settings() );
				}

				restore_current_blog();
			}
		} else {
			$settings = get_option( Settings::OPTION_KEY, array() );
			if ( empty( $settings ) ) {
				// update settings
				update_option( Settings::OPTION_KEY, $settings_obj->get_default_settings() );
			}
		}
	}

	/**
	 * Plugin deactivation hook
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function deactivate() {
	}

	/**
	 * Load plugin classes
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function load_classes() {
		if ( is_admin() ) {
			$this->container['assets'] = new Assets();
			$this->container['menu']   = new Admin\Menu();
		}

		// Load Rest Controllers
		$this->container['rest'] = new Controller();
		// load settings helper
		$this->container['settings'] = new Settings();
	}


	/**
	 * Load plugin hooks
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function load_hooks() {
		add_action( 'init', array( $this, 'localization_setup' ) );
		add_action( 'init', array( $this, 'script_translations' ) );
	}

	/**
	 * Initialize plugin for localization
	 *
	 * @since 1.0.0
	 *
	 * @uses  load_plugin_textdomain()
	 *
	 * @return void
	 */
	public function localization_setup() {
		load_plugin_textdomain( 'nu-vue', false, dirname( plugin_basename( NURULUMBHIYA_PLUGIN_FILE ) ) . '/languages/' );
	}

	/**
	 * Load plugin script translations
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function script_translations() {
		wp_set_script_translations( helper()->get_prefix(), 'nu-vue', $this->get_dir() . '/languages' );
	}

	/**
	 * Get plugin version
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_version(): string {
		return self::$instance->version;
	}

	/**
	 * Get plugin file location
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_file(): string {
		return NURULUMBHIYA_PLUGIN_FILE;
	}

	/**
	 * Get plugin dir location
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_dir(): string {
		return NURULUMBHIYA_PLUGIN_DIR;
	}
}
