<?php

namespace NurulUmbhiya\VueApp\Admin;

// don't call the file directly
use function NurulUmbhiya\VueApp\helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register admin menu
 *
 * @since   1.0.0
 *
 * @package NurulUmbhiya\VueApp\Admin
 */
class Menu {

	/**
	 * Menu slug for the project
	 *
	 * @since 1.0.0
	 *
	 * @var string $menu_slug
	 */
	private string $menu_slug;

	/**
	 * Class constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$this->menu_slug = helper()->get_prefix();
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
	}

	/**
	 * Register admin menu
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function add_admin_menu() {
		global $menu;

		// Add menu
		$vue_js_app = add_menu_page(
			__( 'Nurul Umbhiya - VueJS Developer Applicant Challenge', 'nu-vue' ),
			__( 'Nurul Umbhiya', 'nu-vue' ),
			$this->menu_capability(),
			$this->menu_slug,
			array( $this, 'load_content' ),
			'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNjEuNzYgMjI2LjY5IiAgeG1sbnM6dj0iaHR0cHM6Ly92ZWN0YS5pby9uYW5vIj48cGF0aCBkPSJNMTYxLjA5Ni4wMDFsLTMwLjIyNSA1Mi4zNTFMMTAwLjY0Ny4wMDFILS4wMDVsMTMwLjg3NyAyMjYuNjg4TDI2MS43NDkuMDAxeiIgZmlsbD0iIzQxYjg4MyIvPjxwYXRoIGQ9Ik0xNjEuMDk2LjAwMWwtMzAuMjI1IDUyLjM1MUwxMDAuNjQ3LjAwMUg1Mi4zNDZsNzguNTI2IDEzNi4wMUwyMDkuMzk4LjAwMXoiIGZpbGw9IiMzNDQ5NWUiLz48L3N2Zz4=',
			$this->menu_position()
		);

		// fix menu url since this is a single menu plugin
		foreach ( $menu as &$menu_item ) {
			if ( ! in_array( $this->menu_slug, $menu_item ) ) {
				continue;
			}
			$menu_item[2] = "admin.php?page={$this->menu_slug}#/";
			break;
		}

		// call required hooks
		add_action( "load-$vue_js_app", array( $this, 'remove_admin_notice' ) );
		add_action( "load-$vue_js_app", array( $this, 'load_plugin_assets' ) );
	}

	/**
	 * Remove admin notice
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function remove_admin_notice() {
		remove_all_actions( 'admin_notices' );
	}

	/**
	 * Load plugin assets
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function load_plugin_assets() {
		add_action(
			'admin_enqueue_scripts',
			function () {
				wp_enqueue_script( helper()->get_prefix() );
				wp_enqueue_style( helper()->get_prefix() );
				do_action( 'nurulumbhiya_vueapp_admin_enqueue_scripts' );
			}
		);
	}

	/**
	 * Load Menu Content
	 *
	 * @return void
	 */
	public function load_content() {
		echo '<div id="vue-app-root"></div>';

		$no_script_message = esc_html__( 'Please enable JavaScript to view the app.', 'nu-vue' );
		echo <<<HTML
<noscript>
	<div class="wrap">
		<div class="notice notice-error">
			<p>$no_script_message</p>
		</div>
	</div>
</noscript>
HTML;
	}

	/**
	 * Get menu position
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function menu_position(): string {
		return apply_filters( 'nurulumbhiya_vueapp_menu_position', '100' );
	}

	/**
	 * Get menu capability
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function menu_capability(): string {
		return apply_filters( 'nurulumbhiya_vueapp_menu_capability', 'manage_options' );
	}
}
