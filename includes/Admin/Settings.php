<?php

namespace NurulUmbhiya\VueApp\Admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Settings
 *
 * @package NurulUmbhiya\VueApp
 */
class Settings {

	/**
	 * Option key
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	const OPTION_KEY = 'nurul_umbhiya_vueapp_options';

	/**
	 * Plugin settings
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	private array $settings = array();

	/**
	 * Get settings
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_settings(): array {
		if ( empty( $this->settings ) ) {
			$this->settings = get_option( self::OPTION_KEY, array() );
		}

		return $this->settings;
	}

	/**
	 * Get default settings
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_default_settings(): array {
		return array(
			'no_of_rows'          => 5,
			'human_readable_date' => true,
			'emails'              => array(
				get_option( 'admin_email' ),
			),
		);
	}

	/**
	 * Get setting
	 *
	 * @since 1.0.0
	 *
	 * @param string $key
	 *
	 * @return mixed
	 */
	public function get_setting( string $key ) {
		return $this->settings[ $key ] ?? null;
	}

	/**
	 * Update settings
	 *
	 * @since 1.0.0
	 *
	 * @param array $settings
	 *
	 * @return bool
	 */
	public function update_settings( array $settings ): bool {
		$this->settings = $settings;

		return update_option( self::OPTION_KEY, $settings );
	}

	/**
	 * Get emails
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_emails(): array {
		return $this->settings['emails'] ?? array( get_option( 'admin_email' ) );
	}

	/**
	 * Get no of rows
	 *
	 * @since 1.0.0
	 *
	 * @return int
	 */
	public function get_no_rows(): int {
		return $this->settings['no_of_rows'] ?? 5;
	}

	/**
	 * Get human-readable date is enabled or not
	 *
	 * @since 1.0.0
	 *
	 * @return bool
	 */
	public function get_human_readable_date(): bool {
		return $this->settings['human_readable_date'] ?? false;
	}
}
