<?php

namespace NurulUmbhiya\VueApp\Traits;

/**
 * Trait ChainableContainer
 *
 * @since   1.0.0
 *
 * @package NurulUmbhiya\VueApp\Traits
 */
trait ChainableContainer {

	/**
	 * Contains chainable class instances
	 *
	 * @var array
	 */
	protected array $container = array();

	/**
	 * Magic getter to get chainable container instance
	 *
	 * @since 1.0.0
	 *
	 * @param string $prop
	 *
	 * @return mixed
	 */
	public function __get( string $prop ) {
		if ( array_key_exists( $prop, $this->container ) ) {
			return $this->container[ $prop ];
		}
	}
}
