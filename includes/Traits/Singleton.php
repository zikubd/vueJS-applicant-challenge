<?php
namespace NurulUmbhiya\VueApp\Traits;

/**
 * Trait Singleton
 *
 * @since 1.0.0
 *
 * @package NurulUmbhiya\VueApp\Traits
 */
trait Singleton {
	/**
	 * Singleton class instance holder
	 *
	 * @since 1.0.0
	 *
	 * @var static $instance
	 */
	protected static $instance;

	/**
	 * Class constructor
	 *
	 * @since 1.0.0
	 */
	private function __construct() {
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		$message = ' Backtrace: ' . wp_debug_backtrace_summary();
		_doing_it_wrong( __METHOD__, $message . __( 'Cloning is forbidden.', 'nu-vue' ), $this->get_version() );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		$message = ' Backtrace: ' . wp_debug_backtrace_summary();
		_doing_it_wrong( __METHOD__, $message . __( 'Unserializing instances of this class is forbidden.', 'nu-vue' ), $this->get_version() );
	}

	/**
	 * Make a class instance
	 *
	 * @since 1.0.0
	 *
	 * @return static
	 */
	public static function instance() {
		if ( ! isset( static::$instance ) && ! ( static::$instance instanceof static ) ) {
			static::$instance = new static();

			if ( method_exists( static::$instance, 'boot' ) ) {
				static::$instance->boot();
			}
		}

		return static::$instance;
	}
}
