<?php
namespace NurulUmbhiya\VueApp\Traits;

/**
 * Trait AssetHelper
 *
 * @since 1.0.0
 *
 * @package NurulUmbhiya\VueApp\Traits
 */
trait AssetHelper {
	/**
	 * Get plugin includes dir location
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_include_dir(): string {
		return $this->get_dir() . '/includes/';
	}

	/**
	 * Get plugin lib dir location
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_build_dir(): string {
		return $this->get_dir() . '/build/';
	}

	/**
	 * Get plugin lib dir location
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_build_url(): string {
		return plugins_url( 'build', $this->get_file() );
	}

	/**
	 * Get plugin assets url locations
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_asset_url(): string {
		return plugins_url( 'assets', $this->get_file() );
	}
}
