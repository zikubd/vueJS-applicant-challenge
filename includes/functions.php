<?php

namespace NurulUmbhiya\VueApp;

/**
 * Get Plugin instance
 *
 * @since 1.0.0
 *
 * @return Bootstrap
 */
function app(): Bootstrap {
	return Bootstrap::instance();
}

/**
 * Get Helper Class Instance
 *
 * @since 1.0.0
 *
 * @return Helper
 */
function helper(): Helper {
	return Helper::instance();
}
