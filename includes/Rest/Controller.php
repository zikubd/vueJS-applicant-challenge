<?php
namespace NurulUmbhiya\VueApp\Rest;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Controller
 *
 * @since 1.0.0
 */
class Controller {

	/**
	 * List of controllers
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	private array $controllers;

	/**
	 * Controller constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		// set rest controllers
		$this->controllers = array(
			new SettingsController(),
			new DataController(),
		);

		// Init Rest API routes.
		add_action( 'rest_api_init', array( $this, 'register_rest_routes' ), 10 );
	}

	/**
	 * Register Rest API routes.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register_rest_routes() {
		foreach ( $this->controllers as $controller ) {
			$controller->register_routes();
		}
	}
}
