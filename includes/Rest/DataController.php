<?php

namespace NurulUmbhiya\VueApp\Rest;

use WP_Error;
use WP_REST_Controller;
use WP_REST_Request;
use WP_REST_Response;
use WP_REST_Server;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class DataController
 *
 * @package NurulUmbhiya\VueApp\Rest
 */
class DataController extends WP_REST_Controller {
	/**
	 * Endpoint namespace.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	protected $namespace = 'nurul-umbhiya-vueapp/v1';

	/**
	 * Route base
	 *
	 * @var string
	 */
	protected $rest_base = 'data';

	/**
	 * Register routes.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register_routes() {
		// register chart endpoint
		register_rest_route(
			$this->namespace,
			'/' . $this->rest_base,
			array(
				array(
					'methods'             => WP_REST_Server::READABLE,
					'args'                => $this->get_collection_params(),
					'callback'            => array( $this, 'get_items' ),
					'permission_callback' => array( $this, 'get_items_permissions_check' ),
				),
				'schema' => array( $this, 'get_item_schema' ),
			)
		);
	}

	/**
	 * Checks if a given request has access to get items.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 *
	 * @return bool true, if the request has read access, false otherwise.
	 */
	public function get_items_permissions_check( $request ): bool {
		return current_user_can( 'manage_options' );
	}

	/**
	 * Get API Data.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response|WP_Error
	 */
	public function get_items( $request ) {
		$transient_key = 'nurulumbhiya_vue_app_data';

		// invalidate cache if requested
		if ( rest_is_boolean( $request->get_param( 'invalidate_cache' ) ) && $request->get_param( 'invalidate_cache' ) ) {
			delete_transient( $transient_key );
		}

		// check if data exists on cache
		$data = get_transient( $transient_key );
		if ( false === $data ) {
			$data = $this->get_data_from_external_api();
			if ( is_wp_error( $data ) ) {
				return rest_ensure_response( $data );
			}

			$data = $this->prepare_items_for_response( $data, $request );

			set_transient( $transient_key, $data, MINUTE_IN_SECONDS * 60 );
		}

		switch ( $request->get_param( 'type' ) ) {
			case 'table':
				$data = $data['table'];
				break;
			case 'graph':
				$data = $data['graph'];
				break;
		}

		return rest_ensure_response( $data );
	}

	/**
	 * Prepare data for response.
	 *
	 * @since 1.0.0
	 *
	 * @param array          $data
	 * @param WP_REST_Request $request
	 *
	 * @return array
	 */
	public function prepare_items_for_response( $data, $request ): array {
		$wp_date_time_format = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
		$wp_date_format      = get_option( 'date_format' );
		$prepared_data       = array(
			'table'  => $data['table'],
			'_links' => $this->prepare_links(),
		);

		$prepared_data['table']['data']['rows'] = array_map(
			function ( $row ) use ( $wp_date_time_format ) {
				$row['formatted_date'] = current_datetime()->setTimestamp( $row['date'] )->format( $wp_date_time_format );

				return $row;
			},
			$data['table']['data']['rows']
		);

		$prepared_data['graph'] = array_map(
			function ( $row ) use ( $wp_date_format ) {
				$row['formatted_date'] = current_datetime()->setTimestamp( $row['date'] )->format( $wp_date_format );

				return $row;
			},
			$data['graph']
		);

		return $prepared_data;
	}

	/**
	 * This method will get the data from external API.
	 *
	 * @since 1.0.0
	 *
	 * @return WP_Error|array  json decoded assoc $response_body if api call is success, otherwise throws an WP_Error object
	 */
	private function get_data_from_external_api() {
		$response = wp_remote_get( 'https://miusage.com/v1/challenge/2/static/' ); //phpcs:ignore
		if ( is_wp_error( $response ) ) {
			return $response;
		}

		$response_body = json_decode( wp_remote_retrieve_body( $response ), true );
		//check if valid json
		if ( ! json_last_error() === JSON_ERROR_NONE ) {
			return new WP_Error(
				'invalid-json-error',
				__( 'Invalid JSON String: Could not parse response body.', 'nu-vue' ),
				array( 'response_body' => $response_body )
			);
		}

		if ( isset( $response_body['error'] ) ) {
			return new WP_Error(
				'api-error',
				$response_body['message'] ?? $response_body['error'],
				array(
					'status' => $response_body['status'] ?? 404,
				)
			);
		}

		if ( ! isset( $response_body['graph'] ) || ! isset( $response_body['table'] ) ) {
			return new WP_Error(
				'invalid-json-error',
				__( 'Invalid API Response. Please verify that you\'ve used the right API endpoint.', 'nu-vue' ),
				array( 'response_body' => $response_body )
			);
		}

		return $response_body;
	}

	/**
	 * Prepares links for the request.
	 *
	 * @since 1.0.0
	 *
	 * @return array Links for the given post.
	 */
	protected function prepare_links(): array {
		// Entity meta.
		$links = array(
			'self'             => array(
				'href' => rest_url( sprintf( '%s/%s', $this->namespace, $this->rest_base ) ),
			),
			'table'            => array(
				'href' => rest_url( sprintf( '%s/%s/%s', $this->namespace, $this->rest_base, '?type=table' ) ),
			),
			'graph'            => array(
				'href' => rest_url( sprintf( '%s/%s/%s', $this->namespace, $this->rest_base, '?type=graph' ) ),
			),
			'invalidate_cache' => array(
				'href' => rest_url( sprintf( '%s/%s/%s', $this->namespace, $this->rest_base, '?invalidate_cache=true' ) ),
			),
		);

		return $links;
	}

	/**
	 * Schema for data collections
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_collection_params(): array {
		return array(
			'context'          => $this->get_context_param(),
			'type'             => array(
				'description' => __( 'Types of data to return', 'nu-vue' ),
				'type'        => 'string',
				'enum'        => array( 'graph', 'table', '' ),
				'default'     => '',
				'context'     => array( 'view' ),
			),
			'invalidate_cache' => array(
				'description' => __( 'Invalidate cache', 'nu-vue' ),
				'type'        => 'boolean',
				'default'     => false,
				'context'     => array( 'view' ),
			),
		);
	}

	/**
	 * Get the data collection schema, conforming to JSON Schema.
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_item_schema(): array {
		// returned cached copy whenever available.
		if ( $this->schema ) {
			return $this->add_additional_fields_schema( $this->schema );
		}

		$schema = array(
			'$schema'    => 'http://json-schema.org/draft-04/schema#',
			'title'      => 'data_collection',
			'type'       => 'object',
			'properties' => array(
				'graph' => array(
					'description' => __( 'Graph data to display to the users.', 'nu-vue' ),
					'type'        => 'array',
					'context'     => array( 'view' ),
					'readonly'    => true,
					'items'       => array(
						'type'       => 'object',
						'properties' => array(
							'date'           => 'integer',
							'value'          => 'number',
							'formatted_date' => 'string',
						),
					),
				),
				'table' => array(
					'description' => __( 'Table data to display to the users.', 'nu-vue' ),
					'type'        => 'object',
					'context'     => array( 'view' ),
					'readonly'    => true,
					'properties'  => array(
						'title' => array(
							'type' => 'string',
						),
						'data'  => array(
							'type'       => 'object',
							'properties' => array(
								'headers' => array(
									'type'  => 'array',
									'items' => array(
										'type' => 'string',
									),
								),
								'rows'    => array(
									'type'  => 'array',
									'items' => array(
										'type'       => 'object',
										'properties' => array(
											'id'        => array(
												'type' => 'integer',
											),
											'url'       => array(
												'type'   => 'string',
												'format' => 'uri',
											),
											'title'     => array(
												'type' => 'string',
											),
											'pageviews' => array(
												'type' => 'integer',
											),
											'date'      => array(
												'type' => 'integer',
											),
											'formatted_date' => array(
												'type' => 'string',
											),
										),
									),
								),
							),
						),
					),
				),
			),
		);

		// cache generated schema on endpoint instance.
		$this->schema = $schema;

		return $this->add_additional_fields_schema( $this->schema );
	}
}
