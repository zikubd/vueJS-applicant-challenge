<?php

namespace NurulUmbhiya\VueApp\Rest;

use WP_Error;
use WP_REST_Controller;
use WP_REST_Request;
use WP_REST_Response;
use WP_REST_Server;
use function NurulUmbhiya\VueApp\helper;
use function NurulUmbhiya\VueApp\app;

/**
 * Class SettingsController
 *
 * @package NurulUmbhiya\VueApp\Rest
 */
class SettingsController extends WP_REST_Controller {
	/**
	 * Endpoint namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'nurul-umbhiya-vueapp/v1';

	/**
	 * Route base.
	 *
	 * @var string
	 */
	protected $rest_base = 'settings';

	/**
	 * Register all routes related with seller badge.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register_routes() {
		// register settings endpoint
		register_rest_route(
			$this->namespace,
			'/' . $this->rest_base,
			array(
				array(
					'methods'             => WP_REST_Server::READABLE,
					'callback'            => array( $this, 'get_items' ),
					'permission_callback' => array( $this, 'get_items_permissions_check' ),
				),
				'schema' => array( $this, 'get_item_schema' ),
			)
		);

		// register chart endpoint
		register_rest_route(
			$this->namespace,
			'/' . $this->rest_base . '/(?P<field_name>[\w-]+)/',
			array(
				array(
					'methods'             => WP_REST_Server::CREATABLE,
					'callback'            => array( $this, 'update_item' ),
					'permission_callback' => array( $this, 'update_item_permissions_check' ),
					'args'                => array(
						'field_name' => array(
							'required' => true,
							'type'     => 'string',
							'enum'     => array( 'no_of_rows', 'human_readable_date', 'emails' ),
						),
						'value'      => array(
							'required'          => true,
							'type'              => 'mixed',
							'validate_callback' => array( $this, 'validate_option_values' ),
						),
					),
				),
				'schema' => array( $this, 'get_item_schema' ),
			)
		);
	}

	/**
	 * Update item permission check.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return bool
	 */
	public function get_items_permissions_check( $request ): bool {
		return current_user_can( 'manage_options' );
	}

	/**
	 * Update item permission check
	 *
	 * @since 1.0.0
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return bool
	 */
	public function update_item_permissions_check( $request ): bool {
		return current_user_can( 'manage_options' );
	}

	/**
	 * Get settings data.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response|WP_Error
	 */
	public function get_items( $request ) {
		$settings = app()->settings->get_settings();
		if ( empty( $settings ) ) {
			// get default settings
			$settings = app()->settings->get_default_settings();
			// update settings
			app()->settings->update_settings( $settings );
		}

		$response = $this->prepare_item_for_response( $settings, $request );

		return rest_ensure_response( $response );
	}

	/**
	 * Update settings data.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response|WP_Error
	 */
	public function update_item( $request ) {
		$settings = app()->settings->get_settings();
		if ( empty( $settings ) ) {
			$settings = app()->settings->get_default_settings();
		}

		$settings[ $request['field_name'] ] = $request['value'];

		app()->settings->update_settings( $settings );

		$response = $this->prepare_item_for_response( $settings, $request );

		return rest_ensure_response( $response );
	}

	/**
	 * Prepare item for response.
	 *
	 * @since 1.0.0
	 *
	 * @param array           $item
	 * @param WP_REST_Request $request
	 *
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 */
	public function prepare_item_for_response( $item, $request ) {
		$settings['emails'] = array_filter(
			$item['emails'],
			function ( $email ) {
				return is_email( $email );
			}
		);

		$settings['no_of_rows']          = absint( $item['no_of_rows'] );
		$settings['human_readable_date'] = helper()->string_to_bool( $item['human_readable_date'] );

		return rest_ensure_response( $settings );
	}

	/**
	 * This method will verify the option values before updating.
	 *
	 * @since 1.0.0
	 *
	 * @param $value
	 * @param $request WP_REST_Request
	 * @param $key
	 *
	 * @return bool|WP_Error
	 */
	public function validate_option_values( $value, $request, $key ) {
		switch ( $request['field_name'] ) {
			case 'no_of_rows':
				if ( ! is_numeric( $value ) ) {
					// translators: 1) argument name, 2) argument value
					return new WP_Error( 'rest_invalid_param', sprintf( esc_html__( '%1$s is not of type %2$s', 'nu-vue' ), $key, 'integer' ), array( 'status' => 400 ) );
				}
				break;
			case 'human_readable_date':
				if ( ! rest_is_boolean( $value ) ) {
					// translators: 1) argument name, 2) argument value
					return new WP_Error( 'rest_invalid_param', sprintf( esc_html__( '%1$s is not of type %2$s', 'nu-vue' ), $key, 'boolean' ), array( 'status' => 400 ) );
				}
				break;
			case 'emails':
				if ( ! is_array( $value ) ) {
					// translators: 1) argument name, 2) argument value
					return new WP_Error( 'rest_invalid_param', sprintf( esc_html__( '%1$s is not of type %2$s', 'nu-vue' ), $key, 'array' ), array( 'status' => 400 ) );
				}

				foreach ( $value as $email ) {
					if ( ! is_email( $email ) ) {
						// translators: 1) argument name, 2) argument value
						return new WP_Error( 'rest_invalid_param', sprintf( esc_html__( '%1$s is not a valid email address', 'nu-vue' ), $email ), array( 'status' => 400 ) );
					}
				}
				break;
		}

		return true;
	}

	/**
	 * Get the data collection schema, conforming to JSON Schema.
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_item_schema(): array {
		// returned cached copy whenever available.
		if ( $this->schema ) {
			return $this->add_additional_fields_schema( $this->schema );
		}

		$schema = array(
			'$schema'    => 'http://json-schema.org/draft-04/schema#',
			'title'      => 'settings_collection',
			'type'       => 'object',
			'properties' => array(
				'no_of_rows'          => array(
					'description' => __( 'Number of row items.', 'nu-vue' ),
					'type'        => 'integer',
					'context'     => array( 'view', 'edit' ),
					'default'     => 5,
				),
				'human_readable_date' => array(
					'description' => __( 'Display human readable date.', 'nu-vue' ),
					'type'        => 'boolean',
					'context'     => array( 'view', 'edit' ),
					'default'     => true,
				),
				'emails'              => array(
					'description' => __( 'Email lists to display below table.', 'nu-vue' ),
					'type'        => 'array',
					'context'     => array( 'view', 'edit' ),
					'items'       => array(
						'type'   => 'string',
						'format' => 'email',
					),
				),
			),
		);

		// cache generated schema on endpoint instance.
		$this->schema = $schema;

		return $this->add_additional_fields_schema( $this->schema );
	}
}
