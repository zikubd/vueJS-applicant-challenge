<?php
namespace NurulUmbhiya\VueApp;

use NurulUmbhiya\VueApp\Traits\Singleton;

// don't call the file directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Will hold various helper methods
 *
 * @since 1.0.0
 *
 * @package NurulUmbhiya\VueApp
 */
class Helper {

	use Singleton;

	/**
	 * Plugin prefix
	 *
	 * @since 1.0.0
	 *
	 * @var string $plugin_prefix
	 */
	private string $plugin_prefix = 'nurul_umbhiya_vueapp';

	/**
	 * Add prefix to a string
	 *
	 * @since 1.0.0
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public function add_prefix( string $str = '' ): string {
		return ! empty( $str ) ? $this->plugin_prefix . '_' . $str : $this->get_prefix();
	}

	/**
	 * Get plugin prefix
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function get_prefix(): string {
		return $this->plugin_prefix;
	}

	/**
	 * Converts a string (e.g. 'yes' or 'no') to a bool.
	 *
	 * @since 1.0.0
	 *
	 * @param string|bool $value String to convert. If a bool is passed, it will be returned as-is.
	 *
	 * @return bool
	 */
	public function string_to_bool( $value ): bool {
		$value = $value ?? '';
		return is_bool( $value ) ? $value : ( 'yes' === strtolower( $value ) || 1 === $value || 'true' === strtolower( $value ) || '1' === $value );
	}
}
