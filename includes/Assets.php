<?php

namespace NurulUmbhiya\VueApp;

// don't call the file directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register and Load Plugin Assets
 *
 * @since   1.0.0
 *
 * @package NurulUmbhiya\VueApp
 */
class Assets {
	/**
	 * Class constructor
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_scripts' ) );
		add_action( 'init', array( $this, 'localize_scripts' ) );
	}

	/**
	 * Register Scripts
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register_scripts() {
		$asset_path   = app()->get_build_dir() . '/main.asset.php';
		$version      = app()->version;
		$dependencies = array();

		if ( file_exists( $asset_path ) ) {
			$asset        = require_once $asset_path;
			$version      = is_array( $asset ) && isset( $asset['version'] )
				? $asset['version']
				: $version;
			$dependencies = is_array( $asset ) && isset( $asset['dependencies'] )
				? $asset['dependencies']
				: $dependencies;
		}

		wp_register_script(
			helper()->get_prefix(),
			app()->get_build_url() . '/main.js',
			$dependencies,
			$version,
			true
		);

		wp_register_style(
			helper()->get_prefix(),
			app()->get_build_url() . '/main.css',
			array(),
			$version
		);
	}

	/**
	 * Localize Scripts
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function localize_scripts() {
		// localize wp_rest nonce
		wp_localize_script(
			helper()->get_prefix(),
			helper()->get_prefix(),
			array(
				'root'        => esc_url_raw( rest_url() ),
				'nonce'       => wp_create_nonce( 'wp_rest' ),
				'admin_email' => get_option( 'admin_email' ),
			)
		);
	}
}
