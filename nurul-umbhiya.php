<?php
/**
 * Plugin Name: Nurul Umbhiya - VueJS Developer Applicant Challenge
 * Plugin URI: https://github.com/nurul-umbhiya/vueJS-applicant-challenge
 * Description: VueJS Developer Applicant Challenge.
 * Version: 1.0.1
 * Author: Nurul Umbhiya
 * Author URI: https://www.linkedin.com/in/nurul-umbhiya/
 * Text Domain: nu-vue
 * Domain Path: /languages/
 * License: GPL2
 */

/*
 * Copyright (c) 2024 Nurul Umbhiya (email: zikubd@gmail.com). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA
 * **********************************************************************
 */


// define constants
if ( ! defined( 'NURULUMBHIYA_PLUGIN_FILE' ) ) {
	define( 'NURULUMBHIYA_PLUGIN_FILE', __FILE__ );
}

if ( ! defined( 'NURULUMBHIYA_PLUGIN_DIR' ) ) {
	define( 'NURULUMBHIYA_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// check for php version
if ( version_compare( '7.4', phpversion(), '>=' ) ) {
	add_action(
		'admin_notices',
		function () {
			?>
			<div class="notice notice-error">
				<p><?php esc_html_e( 'Nurul Umbhiya Vue App requires PHP 7.4 or higher to function properly. Please upgrade PHP or deactivate the plugin.', 'nu-vue' ); ?></p>
			</div>
			<?php
		}
	);
	return;
}

// check for WordPress version
if ( version_compare( '5.9', get_bloginfo( 'version' ), '>=' ) ) {
	add_action(
		'admin_notices',
		function () {
			?>
			<div class="notice notice-error">
				<p><?php esc_html_e( 'Nurul Umbhiya Vue App requires WordPress 5.9 or higher to function properly. Please upgrade WordPress or deactivate the plugin.', 'nu-vue' ); ?></p>
			</div>
			<?php
		}
	);
	return;
}

// return if composer autoload file not found
if ( ! file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
	return;
}

// autoload composer files
require_once __DIR__ . '/vendor/autoload.php';


// instantiate the plugin
NurulUmbhiya\VueApp\app();
