# Copyright (c) 2024 Nurul Umbhiya. All Rights Reserved.
msgid ""
msgstr ""
"Project-Id-Version: Nurul Umbhiya - VueJS Developer Applicant Challenge 1.0.1\n"
"Report-Msgid-Bugs-To: https://nurul.me/\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2024-03-03T15:13:15+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.8.1\n"
"X-Domain: nu-vue\n"

#. Plugin Name of the plugin
#: includes/Admin/Menu.php:52
msgid "Nurul Umbhiya - VueJS Developer Applicant Challenge"
msgstr ""

#. Plugin URI of the plugin
msgid "https://github.com/nurul-umbhiya/vueJS-applicant-challenge"
msgstr ""

#. Description of the plugin
msgid "VueJS Developer Applicant Challenge."
msgstr ""

#. Author of the plugin
#: includes/Admin/Menu.php:53
msgid "Nurul Umbhiya"
msgstr ""

#. Author URI of the plugin
msgid "https://www.linkedin.com/in/nurul-umbhiya/"
msgstr ""

#: includes/Admin/Menu.php:112
msgid "Please enable JavaScript to view the app."
msgstr ""

#: includes/Rest/DataController.php:173
msgid "Invalid JSON String: Could not parse response body."
msgstr ""

#: includes/Rest/DataController.php:191
msgid "Invalid API Response. Please verify that you've used the right API endpoint."
msgstr ""

#: includes/Rest/DataController.php:237
msgid "Types of data to return"
msgstr ""

#: includes/Rest/DataController.php:244
msgid "Invalidate cache"
msgstr ""

#: includes/Rest/DataController.php:271
msgid "Graph data to display to the users."
msgstr ""

#: includes/Rest/DataController.php:285
msgid "Table data to display to the users."
msgstr ""

#. translators: 1) argument name, 2) argument value
#: includes/Rest/SettingsController.php:195
#: includes/Rest/SettingsController.php:201
#: includes/Rest/SettingsController.php:207
msgid "%1$s is not of type %2$s"
msgstr ""

#. translators: 1) argument name, 2) argument value
#: includes/Rest/SettingsController.php:213
msgid "%1$s is not a valid email address"
msgstr ""

#: includes/Rest/SettingsController.php:241
msgid "Number of row items."
msgstr ""

#: includes/Rest/SettingsController.php:247
msgid "Display human readable date."
msgstr ""

#: includes/Rest/SettingsController.php:253
msgid "Email lists to display below table."
msgstr ""

#: includes/Traits/Singleton.php:36
msgid "Cloning is forbidden."
msgstr ""

#: includes/Traits/Singleton.php:46
msgid "Unserializing instances of this class is forbidden."
msgstr ""

#: nurul-umbhiya.php:57
msgid "Nurul Umbhiya Vue App requires PHP 7.4 or higher to function properly. Please upgrade PHP or deactivate the plugin."
msgstr ""

#: nurul-umbhiya.php:72
msgid "Nurul Umbhiya Vue App requires WordPress 5.9 or higher to function properly. Please upgrade WordPress or deactivate the plugin."
msgstr ""

#: build/384.js:1
msgid "Bar Chart"
msgstr ""

#: build/384.js:1
msgid "Date"
msgstr ""

#: build/384.js:1
msgid "Value"
msgstr ""

#: build/384.js:1
msgid "Graph Data"
msgstr ""

#: build/384.js:1
msgid "A bar chart will be displayed with the API data."
msgstr ""

#: build/452.js:1
msgid "Settings updated."
msgstr ""

#: build/452.js:1
msgid "You can only add 5 emails."
msgstr ""

#: build/452.js:1
msgid "eg: mail@mail.com"
msgstr ""

#: build/452.js:1
msgid "You can specify upto five emails."
msgstr ""

#: build/452.js:1
#: build/main.js:1
msgid "Settings"
msgstr ""

#: build/452.js:1
msgid "Control settings for Table tab view."
msgstr ""

#: build/452.js:1
msgid "No of rows"
msgstr ""

#: build/452.js:1
msgid "Please specify the number of rows you want to display under the table. Note that, you can specify any number between 1 and 5"
msgstr ""

#: build/452.js:1
msgid "Human Readable Date"
msgstr ""

#: build/452.js:1
msgid "Disable this option if you want to display unix timestamp instead of human readable date."
msgstr ""

#: build/452.js:1
#: build/main.js:1
msgid "Emails"
msgstr ""

#: build/main.js:1
msgid "Table"
msgstr ""

#: build/main.js:1
msgid "Graph"
msgstr ""

#: build/main.js:1
msgid "VueJS Applicant Challenge"
msgstr ""

#: build/main.js:1
msgid "Completed by: Nurul Umbhiya"
msgstr ""

#: build/main.js:1
msgid "Top Pages"
msgstr ""

#: build/main.js:1
msgid "Top pages are displayed here."
msgstr ""
