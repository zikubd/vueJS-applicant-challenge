const fs = require('fs-extra');
const replace = require('replace-in-file');

const pluginFiles = [
	'src/**/*',
	'includes/**/*',
	'nurul-umbhiya.php',
	'uninstall.php',
];

const {version} = JSON.parse(fs.readFileSync('package.json'));

replace({
	files: pluginFiles,
	from: /NU_SINCE/g,
	to: version,
});
