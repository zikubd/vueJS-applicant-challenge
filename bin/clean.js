const path = require( 'path' );
const chalk = require( 'chalk' );
const fs = require('fs-extra');

const deleteDirs = [
    'build/*',
    'dist/*.zip',
];

deleteDirs.forEach( ( dir ) => {
    fs.remove( path.resolve( dir ), ( error ) => {
        if ( error ) {
            console.log( error );
        } else {
            console.log( chalk.greenBright( `🗑️Deleted directory - ${dir}.` ) );
        }
    } );
} );
