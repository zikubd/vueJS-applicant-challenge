const fs = require('fs-extra');
const path = require('path');
const { exec } = require('child_process');
const chalk = require('Chalk');
const _ = require('lodash');

const pluginFiles = [
    'build/',
    'includes/',
    'languages/',
    'vendor/',
    'index.php',
    'nurul-umbhiya.php',
    'uninstall.php',
    'composer.json',
];

const removeFiles = ['composer.json', 'composer.lock'];

const { version } = JSON.parse(fs.readFileSync('package.json'));

exec(
    'rm -rf *',
    {
        cwd: 'dist',
    },
    (error) => {
        if (error) {
            console.log(chalk.yellow(`⚠️ Could not find the dist directory.`));
            console.log(chalk.green(`🗂 Creating the dist directory ...`));
            // Making dist folder.
            fs.mkdirp('dist');
        }

        const dest = 'dist/nurul-umbhiya'; // Temporary folder name after coping all the files here.
        fs.mkdirp(dest);

        console.log(`🗜 Started making the zip ...`);
        try {
            console.log(`⚙️ Copying plugin files ...`);

            // Coping all the files into dist folder.
            pluginFiles.forEach((file) => {
                fs.copySync(file, `${dest}/${file}`);
            });
            console.log(`📂 Finished copying files.`);
        } catch (err) {
            console.error(chalk.red('❌ Could not copy plugin files.'), err);
            return;
        }

        exec(
            'composer install --optimize-autoloader --no-dev',
            {
                cwd: dest,
            },
            (error) => {
                if (error) {
                    console.log(
                        chalk.red(
                            `❌ Could not install composer in ${dest} directory.`
                        )
                    );
                    console.log(chalk.bgRed.black(error));

                    return;
                }

                console.log(
                    `⚡️ Installed composer packages in ${dest} directory.`
                );

                // Removing files that is not needed in the production now.
                removeFiles.forEach((file) => {
                    fs.removeSync(`${dest}/${file}`);
                });

                // Output zip file name.
                const zipFile = `nurul-umbhiya-v${version}.zip`;

                console.log(`📦 Making the zip file ${zipFile} ...`);

                // Making the zip file here.
                exec(
                    `zip ${zipFile} nurul-umbhiya -rq`,
                    {
                        cwd: 'dist',
                    },
                    (error) => {
                        if (error) {
                            console.log(
                                chalk.red(`❌ Could not make ${zipFile}.`)
                            );
                            console.log(chalk.bgRed.black(error));

                            return;
                        }

                        fs.removeSync(dest);
                        console.log(chalk.green(`✅  ${zipFile} is ready. 🎉`));
                    }
                );
            }
        );
    }
);
