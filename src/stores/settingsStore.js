import { defineStore } from 'pinia';
import { SETTINGS_STORE } from '@/constants.js';

export const useSettingsStore = defineStore('settingsStore', {
    // state
    state: () => ({
        settings: {},
    }),

    // actions
    actions: {
        setNoOfRows(no_of_rows) {
            this.settings.no_of_rows = no_of_rows;
        },

        setHumanReadableDate(human_readable_date) {
            this.settings.human_readable_date = human_readable_date;
        },

        setEmails(emails) {
            this.settings.emails = emails;
        },

        updateSettings(settings) {
            this.settings = {
                ...this.settings,
                ...settings,
            };
            localStorage.setItem(SETTINGS_STORE, JSON.stringify(this.settings));
        },
    },

    // getters
    getters: {
        getSettings() {
            const settings = localStorage.getItem(SETTINGS_STORE);
            this.settings = settings ? JSON.parse(settings) : {};
            return this.settings;
        },

        getNoOfRows() {
            return this.settings.no_of_rows;
        },

        getHumanReadableDate() {
            return this.settings.human_readable_date;
        },

        getEmails() {
            return this.settings.emails;
        },
    },
});
