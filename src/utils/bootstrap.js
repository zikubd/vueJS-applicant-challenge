// get apiFetch
import apiFetch from '@wordpress/api-fetch';

/**
 * Get global data.
 *
 * @param {string} key
 *
 * @return {Object | string} Global settings related data.
 */
export const getGlobalData = (key = '') => {
	const data =
		typeof window?.nurul_umbhiya_vueapp === 'undefined'
			? {}
			: window?.nurul_umbhiya_vueapp;

	if (undefined === data) {
		return data;
	}

	if (!key.length) {
		return data;
	}

	return data[key];
};

// Set the nonce for the apiFetch
apiFetch.use(apiFetch.createNonceMiddleware(getGlobalData('nonce')));
export const fetchApi = apiFetch;

/**
 * Get the error message from the api response.
 *
 * @param {object} error
 *
 * @returns {string|string[]}
 */
export const getApiErrors = (error) => {
	// If errors are from rest api response.
	if (error?.data?.details) {
		let messageText = [];
		Object.keys(error.data.details).forEach((item) => {
			const value = error.data.details[item];
			console.log(value);
			messageText.push(`${value.code}: ${value.message}`);
		});
		return messageText;
	}

	if (!error?.additional_errors?.length) {
		return error.message;
	}

	const AdditionalErrors = error.additional_errors.map((item) => {
		return item?.message;
	});

	return [error.message, ...AdditionalErrors];
};

/**
 * Print the error message.
 *
 * @param {string|string[]} errors
 *
 * @returns {string}
 */
export const printApiError = (errors) => {
	const messages = Array.isArray(errors)
		? errors.map((item) => `<p>${item}</p>`).join('')
		: errors;

	return `<div>${messages}</div>`;
};

/**
 * Show the snackbar message.
 *
 * @param {string} message
 * @param {string} type
 */
export const showSnackBar = (message, type = 'success') => {
	const snackbar = document.getElementById('snackbar');
	snackbar.innerHTML = message;
	snackbar.className = `show ${type}`;
	setTimeout(() => {
		snackbar.className = snackbar.className.replace(`show ${type}`, '');
	}, 2000);
};
