import { createRouter, createWebHistory } from 'vue-router';
import Table from 'pages/Table.vue';

const router = createRouter({
    history: createWebHistory('#'),
    routes: [
        {
            path: '/',
            name: 'Home',
            redirect: '/table',
        },
        {
            path: '/table',
            name: 'Table',
            component: Table,
        },
        {
            path: '/graph',
            name: 'Graph',
            // route level code-splitting
            // this generates a separate chunk (About.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import('pages/Graph.vue'),
            //component: Graph
        },
        {
            path: '/settings',
            name: 'Settings',
            // route level code-splitting
            // this generates a separate chunk (About.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import('pages/Settings.vue'),
            //component: Settings
        },
    ],
});

export default router;
