# Nurul Umbhiya - VueJS Developer Applicant Challenge


![VueJS Developer Applicant Challenge](https://lh3.googleusercontent.com/pw/AP1GczOaFfdS5az1STnIVugMTQlSu6np_aqYBHyocNwYBhdrnNAmZD8c2fq5L_UmHBRjjiSgJdTQY1jB99JuWY-nApo5WlKZC2oUeol66-uqCBGIuLmStZ6wpUn_c9zorcqooCLREYLBSaI2jE8x3Ucl7ouZ=w800-h740-s-no-gm)


## Installation

1. Clone the repository under `wp-content/plugins` folder
```bash
git clone git@github.com:nurul-umbhiya/vueJS-applicant-challenge.git nurul-umbhiya
```
2. Run `cd nurul-umbhiya` to navigate to the plugin folder
2. Run `composer install` to install the dependencies
2. Run `npm install` to install the dependencies
3. Run `npm run build` to build the development server

### All available commands

- Dev build.
```
npm run start
```
- Production build.
```
npm run build
```
- Update all npm packages.
```
npm run packages-update
```
- Replace all `NU_SINCE` version into a current version from package.json.
```
npm run version
```
- Make or regenerate pot file.
```
npm run makepot
```
- Removes the build folder and its contents.
```
npm run clean
```
- Creates zip file
```
npm run plugin-zip
```
- Makes a production build zip of plugin.
```
npm run release
```
