<?php
// Exit if accessed directly.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

if ( is_multisite() ) {
	$site_ids = get_sites( array( 'fields' => 'ids' ) ); //phpcs:ignore
	// Get all blog ids
	foreach ( $site_ids as $id ) { // phpcs:ignore
		switch_to_blog( $id );

		// Delete options
		delete_option( 'nurul_umbhiya_vueapp_options' );

		restore_current_blog();
	}
} else {
	// Delete options
	delete_option( 'nurul_umbhiya_vueapp_options' );
}
