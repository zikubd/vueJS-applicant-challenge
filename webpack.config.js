const webpack = require('webpack');
const path = require('path');
const defaultConfig = require('@wordpress/scripts/config/webpack.config');

const { VueLoaderPlugin } = require('vue-loader');

module.exports = (env) => {
    return {
        ...defaultConfig,
        entry: {
            ...defaultConfig.entry,
            main: './src/main.js',
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, 'build'),
        },
        resolve: {
            ...defaultConfig.resolve,
            alias: {
                ...defaultConfig.resolve.alias,
                '@': path.resolve('./src/'),
                components: path.resolve('./src/components/'),
                pages: path.resolve('./src/pages/'),
                assets: path.resolve('./src/assets/'),
            },
        },
        module: {
            ...defaultConfig.module,
            rules: [
                ...defaultConfig.module.rules,
                {
                    test: /\.vue$/,
                    use: 'vue-loader',
                },
                {
                    test: /\.svg/,
                    type: 'asset/inline',
                },
                {
                    test: /\.(sc|sa)ss$/,
                    loader: 'sass-loader',
                    options: {
                        additionalData: `@import "@/assets/scss/_include-media.scss";@import "@/assets/scss/_variables.scss";`,
                    },
                },
            ],
        },
        externals: {
            jquery: 'jQuery',
        },
        plugins: [
            ...defaultConfig.plugins,
            new VueLoaderPlugin(),
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: false,
                __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: false,
            }),
        ],
    };
};
